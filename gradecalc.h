#ifndef GRADECALC_H
#define GRADECALC_H

#include <QMainWindow>

namespace Ui {
class GradeCalc;
}

class GradeCalc : public QMainWindow
{
    Q_OBJECT

public:
    explicit GradeCalc(QWidget *parent = 0);
    ~GradeCalc();

private slots:
    void on_hw1Slider_valueChanged(int value);

    void on_hw1Spin_valueChanged(int arg1);

private:
    Ui::GradeCalc *ui;
};

#endif // GRADECALC_H
